import { Component, ViewChild } from '@angular/core';
import { Platform,Nav } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { HomePage } from '../pages/home/home';
import {LoginPage} from '../pages/login/login';
import { ApiProvider } from '../providers/api/api';
import {ProfilePage} from '../pages/profile/profile';
import { Testhistory } from '../pages/testhistory/testhistory';
import { FeedbackPage } from '../pages/feedback/feedback';


@Component({
  templateUrl: 'app.html',
  providers : [ApiProvider]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;
  constructor(platform: Platform, public service:ApiProvider) {
    platform.ready().then(() => {
                  StatusBar.styleDefault();
                  Splashscreen.hide();
                });
  }

  home(){
    this.nav.setRoot(HomePage);
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }

}
