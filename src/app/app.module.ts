import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import {SelecttestPage} from '../pages/selecttest/selecttest';
import {SelecttesturduPage} from '../pages/selecttesturdu/selecttesturdu';
import {SelecttesthindiPage} from '../pages/selecttesthindi/selecttesthindi';
import {SelecttestpunjabiPage} from '../pages/selecttestpunjabi/selecttestpunjabi';
import {QuestionPage} from '../pages/question/question';
import {UrduquestionPage} from '../pages/urduquestion/urduquestion';
import {HindiquestionPage} from '../pages/hindiquestion/hindiquestion';
import {PunjabiquestionPage} from '../pages/punjabiquestion/punjabiquestion';
import { NativeAudio } from '@ionic-native/native-audio';
import {ThanksPage} from '../pages/thanks/thanks';
import { ApiProvider } from '../providers/api/api';
import { BrowserModule } from '@angular/platform-browser';
import {  HttpModule } from '@angular/http';
import { Media, MediaObject } from '@ionic-native/media';
import { Global } from '../providers/global';
import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SelecttestPage,
    SelecttesturduPage,
    SelecttesthindiPage,
    SelecttestpunjabiPage,
    QuestionPage,
    UrduquestionPage,
    HindiquestionPage,
    PunjabiquestionPage,
    ThanksPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SelecttestPage,
    SelecttesturduPage,
    SelecttesthindiPage,
    SelecttestpunjabiPage,
    QuestionPage,
    UrduquestionPage,
    HindiquestionPage,
    PunjabiquestionPage,
    ThanksPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},
  NativeAudio,
    ApiProvider,
    Media,
    Global
    ]
})
export class AppModule {}
