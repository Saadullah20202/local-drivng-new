import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ApiProvider {

  api_token:any;
  url:any = 'http://dmvtest.localdriving.com/api';
  musicurl:any = 'http://dmvtest.localdriving.com/audio/question/'
  userdataurl:any = this.url+'/users/me?api_token=';

  constructor(public http: Http) {
    console.log('Hello ApiProvider Provider');
    this.api_token = localStorage.getItem('api_token');
  }
    gettest(language){
      return this.http.get(this.url+'/tests?locale='+language);
    }

    gettestquestions(id){
      return this.http.get(this.url+'/tests/'+id+'/questions');
    }

    getuserdata(){
      return this.http.get(this.url+'/users/me?api_token='+this.api_token);
    }


    gettesthistory(){
      return this.http.get(this.url+'/users/me/tests?api_token='+this.api_token);
    }

    getpakage(){
      return this.http.get(this.url+'/settings');
    }
}
